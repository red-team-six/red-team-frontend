export default {
  updateAssessment(state, formData) {
    state.assessment = formData
  },
  updateStats(state, data) {
    state.stats = data
  },
  SOCKET_UPDATE_STATS: (state, data) => {
    state.stats = data
  }
}
