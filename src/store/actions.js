export default {
  saveAssessment({ commit }, formData) {
    commit('updateAssessment', formData)
  }
}
