import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Admin from './views/Admin.vue'
import AdminService from './views/AdminService.vue'
import Reports from './views/Reports.vue'
import Assessment from './views/Assessment.vue'
import Education from './views/Education.vue'
import Food from './views/user-resources/Food.vue'
import FoodService from './views/FoodService.vue'
import Jobs from './views/user-resources/Jobs.vue'
import Housing from './views/user-resources/Housing.vue'

import AdminNav from './components/AdminNav.vue'
import UserNav from './components/UserNav.vue'
import EducationNav from './components/EducationNav.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        header: UserNav,
        default: Home
        // footer: AppFooter
      }
    },
    {
      path: '/about',
      name: 'about',
      components: {
        header: UserNav,
        default: About
        // footer: AppFooter
      }
    },
    {
      path: '/admin',
      name: 'admin',
      components: {
        header: AdminNav,
        default: Admin
        // footer: AppFooter
      }
    },
    {
      path: '/admin/:service',
      name: 'adminService',
      components: {
        header: AdminNav,
        default: AdminService
        // footer: AppFooter
      }
    },
    {
      path: '/reports',
      name: 'reports',
      components: {
        header: AdminNav,
        default: Reports
        // footer: AppFooter
      }
    },
    {
      path: '/education/assessment',
      name: 'assessment',
      components: {
        header: EducationNav,
        default: Assessment
        // footer: AppFooter
      }
    },
    {
      path: '/education',
      name: 'education',
      components: {
        header: EducationNav,
        default: Education
        // footer: AppFooter
      }
    },
    {
      path: '/food',
      name: 'food',
      components: {
        header: UserNav,
        default: Food
        // footer: AppFooter
      }
    },
    {
      path: '/food/:service',
      name: 'foodService',
      components: {
        header: UserNav,
        default: FoodService
        // footer: AppFooter
      }
    },
    {
      path: '/jobs',
      name: 'jobs',
      components: {
        header: UserNav,
        default: Jobs
        // footer: AppFooter
      }
    },
    {
      path: '/housing',
      name: 'housing',
      components: {
        header: UserNav,
        default: Housing
        // footer: AppFooter
      }
    }
  ]
})
