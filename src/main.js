import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import vSelect from 'vue-select'
import FullCalendar from 'vue-full-calendar'

import App from './App.vue'
import VueSocketIO from 'vue-socket.io'
import router from './router'
import store from './store'
import './registerServiceWorker'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-select/src/scss/vue-select.scss'
import 'fullcalendar/dist/fullcalendar.min.css'
import './assets/sass/index.sass'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.component('v-select', vSelect)
Vue.use(FullCalendar)
Vue.use(require('vue-chartist'))

Vue.use(new VueSocketIO({
  debug: true,
  connection: 'https://civic-hackathon-2019.herokuapp.com',
  // connection: 'http://localhost:3000',
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_'
  }
}))

new Vue({
  router,
  store,
  render: h => h(App),
  created () {
    if (sessionStorage.redirect) {
      const redirect = sessionStorage.redirect
      delete sessionStorage.redirect
      this.$router.push(redirect)
    }
  }
}).$mount('#app')
